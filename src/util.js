function factorial(n){
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
};


function div_check(n){
	if(n % 5 === 0 || n % 7 === 0){
		return true;
	}else{
		return false;
	}
};

module.exports = {
	factorial,
	div_check
};

